# :star2: **10分钟编写ado文档** :star2: 

## 1.项目简介
   编写 ado 文件是使用 Stata 过程中基础但非常重要的任务之一。为了让大家快速上手，我们将通过一个简单的例子——计算阶乘，由浅入深地讲解如何逐步编写满足不同需求的 ado 文件。

## 2.项目展示

- 专题：[Stata程序](https://www.lianxh.cn/blogs/26.html)
  - [Stata程序：10 分钟快乐编写 ado 文件](https://www.lianxh.cn/news/a9d7de7ff1d80.html)



## 3.过程文档 Step by Step
- [myfact1——使用 ado 文件计算阶乘](https://gitee.com/arlionn/myfact/wikis/myfact1——使用%20ado%20文件计算阶乘?sort_id=3220377)
- [myfact2——若用户输入的参数不是正整数，报错并提示](https://gitee.com/arlionn/myfact/wikis/myfact2——若用户输入的参数不是正整数%EF%BC%8C报错并提示?sort_id=3220379)
- [myfact3——增加选项 format()，以便用户自定义结果的显示格式](https://gitee.com/arlionn/myfact/wikis/myfact3——增加选项%20format()%EF%BC%8C以便用户自定义结果的显示格式?sort_id=3220381)
- [myfact4——若用户输入的控制数据显示格式的代码错误，报错并提示](https://gitee.com/arlionn/myfact/wikis/myfact4——若用户输入的控制数据显示格式的代码错误%EF%BC%8C报错并提示?sort_id=3220383)
- [myfact5——在保留自定义结果显示格式的基础上，产生新变量](https://gitee.com/arlionn/myfact/wikis/myfact5——在保留自定义结果显示格式的基础上%EF%BC%8C产生新变量?sort_id=3220388)
- [myfact6——利用可选项 rclass，生成并调用返回值](https://gitee.com/arlionn/myfact/wikis/myfact6——利用可选项%20rclass%EF%BC%8C生成并调用返回值?sort_id=3220389)
- [myfact7——多个数值的阶乘](https://gitee.com/arlionn/myfact/wikis/myfact7——多个数值的阶乘?sort_id=3220390)
- [myfact8——直接调用 Mata 自带的函数factorial()计算阶乘](https://gitee.com/arlionn/myfact/wikis/myfact8——直接调用%20Mata%20自带的函数factorial()计算阶乘?sort_id=3220391)
- [myfact9——自行创建 Mata 函数，并在 ado 文件中调用](https://gitee.com/arlionn/myfact/wikis/myfact9——自行创建%20Mata%20函数%EF%BC%8C并在%20ado%20文件中调用?sort_id=3220392)

## 4.最终文档
- [综合各种功能的myfact10.ado文件](https://gitee.com/arlionn/myfact/wikis/综合各种功能的myfact10.ado文件?sort_id=3220394)

## 5.帮助文件
- [帮助文件编写](https://gitee.com/arlionn/myfact/wikis/帮助文件编写?sort_id=3220428)
